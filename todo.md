## Setting up the environment

```
import re  # For preprocessing

import pandas as pd  # For data handling
from time import time  # To time our operations
from collections 
import defaultdict  # For word frequency
import spacy  # For preprocessing
import logging  # Setting up the loggings to monitor gensimlogging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)
```
## Data

### Simpsons dataset
https://www.kaggle.com/ambarish/fun-in-text-mining-with-simpsons/data

## Preprocessing

We keep only two columns:

- raw_character_text: the character who speaks (can be useful when monitoring the preprocessing steps)
- spoken_words: the raw text from the line of dialogue

We do not keep normalized_text because we want to do our own preprocessing.

You can find the resulting file here: https://www.kaggle.com/pierremegret/dialogue-lines-of-the-simpsons

```
df = pd.read_csv('../input/simpsons_dataset.csv')
df.shape
(158314, 2)
```
```
df.head()

raw_character_text	spoken_words
0	Miss Hoover	No, actually, it was a little of both. Sometim...
1	Lisa Simpson	Where's Mr. Bergstrom?
2	Miss Hoover	I don't know. Although I'd sure like to talk t...
3	Lisa Simpson	That life is worth living.
4	Edna Krabappel-Flanders	The polls will be open from now until the end 
```

### Removing the missing values:

```
df.isnull().sum()
raw_character_text    17814
spoken_words          26459
dtype: int64
```
Removing the missing values:
```
df = df.dropna().reset_index(drop=True)
df.isnull().sum()
raw_character_text    0
spoken_words          0
dtype: int64
```

### Cleaning

We are lemmatizing and removing the stopwords and non-alphabetic characters for each line of dialogue.

```
nlp = spacy.load('en', disable=['ner', 'parser']) # disabling Named Entity Recognition for speed

def cleaning(doc):
    # Lemmatizes and removes stopwords
    # doc needs to be a spacy Doc object
    txt = [token.lemma_ for token in doc if not token.is_stop]
    # Word2Vec uses context words to learn the vector representation of a target word,
    # if a sentence is only one or two words long,
    # the benefit for the training is very small
    if len(txt) > 2:
        return ' '.join(txt)

```
### Removes non-alphabetic characters:

```
brief_cleaning = (re.sub("[^A-Za-z']+", ' ', str(row)).lower() for row in df['spoken_words'])
```
Taking advantage of spaCy .pipe() attribute to speed-up the cleaning process:
```

t = time()

txt = [cleaning(doc) for doc in nlp.pipe(brief_cleaning, batch_size=5000, n_threads=-1)]

print('Time to clean up everything: {} mins'.format(round((time() - t) / 60, 2)))
```
Time to clean up everything: 1.91 mins
Put the results in a DataFrame to remove missing values and duplicates:
```
df_clean = pd.DataFrame({'clean': txt})
df_clean = df_clean.dropna().drop_duplicates()
df_clean.shape
(92412, 1)

```
### Bigrams:
We are using Gensim Phrases package to automatically detect common phrases (bigrams) from a list of sentences. https://radimrehurek.com/gensim/models/phrases.html

The main reason we do this is to catch words like "mr_burns" or "bart_simpson" !

```
from gensim.models.phrases import Phrases, Phraser
INFO - 01:16:12: 'pattern' package not found; tag filters are not available for English
```
As Phrases() takes a list of list of words as input:

```
sent = [row.split() for row in df_clean['clean']]
```
Creates the relevant phrases from the list of sentences:

```
phrases = Phrases(sent, min_count=30, progress_per=10000)
INFO - 01:16:13: collecting all words and their counts
INFO - 01:16:13: PROGRESS: at sentence #0, processed 0 words and 0 word types
INFO - 01:16:13: PROGRESS: at sentence #10000, processed 67396 words and 50551 word types
INFO - 01:16:13: PROGRESS: at sentence #20000, processed 140465 words and 95808 word types
INFO - 01:16:13: PROGRESS: at sentence #30000, processed 207950 words and 132011 word types
INFO - 01:16:13: PROGRESS: at sentence #40000, processed 270207 words and 164407 word types
INFO - 01:16:13: PROGRESS: at sentence #50000, processed 334085 words and 196195 word types
INFO - 01:16:13: PROGRESS: at sentence #60000, processed 400877 words and 228659 word types
INFO - 01:16:14: PROGRESS: at sentence #70000, processed 467802 words and 260712 word types
INFO - 01:16:14: PROGRESS: at sentence #80000, processed 534361 words and 292095 word types
INFO - 01:16:14: PROGRESS: at sentence #90000, processed 602037 words and 321944 word types
INFO - 01:16:14: collected 328658 word types from a corpus of 618920 words (unigram + bigrams) and 92412 sentences
INFO - 01:16:14: using 328658 counts as vocab in Phrases<0 vocab, min_count=30, threshold=10.0, max_vocab_size=40000000>
```
The goal of Phraser() is to cut down memory consumption of Phrases(), by discarding model state not strictly needed for the bigram detection task:

```
INFO - 01:16:14: source_vocab length 328658
INFO - 01:16:17: Phraser built with 127 phrasegrams
```
Transform the corpus based on the bigrams detected:

sentences = bigram[sent]
### Most Frequent Words:
Mainly a sanity check of the effectiveness of the lemmatization, removal of stopwords, and addition of bigrams.

```
word_freq = defaultdict(int)
for sent in sentences:
    for i in sent:
        word_freq[i] += 1
len(word_freq)
29673
sorted(word_freq, key=word_freq.get, reverse=True)[:10]
['be', 'not', 'oh', 'will', 'like', "'s", 'know', 'think', 'hey', 'good']
```


## Training the model
We use Gensim implementation of word2vec: https://radimrehurek.com/gensim/models/word2vec.html
```
import multiprocessing
from gensim.models import Word2Vec
```

###  training of the model
1. Word2Vec
2. build_vocab
3. train

**The parameters:**
- min_count = int - Ignores all words with total absolute frequency lower than this - (2, 100)
- window = int - The maximum distance between the current and predicted word within a sentence. E.g. window words on the left and window words on the left of our target - (2, 10)
- size = int - Dimensionality of the feature vectors. - (50, 300)
- sample = float - The threshold for configuring which higher-frequency words are randomly downsampled. Highly influencial. - (0, 1e-5)
- alpha = float - The initial learning rate - (0.01, 0.05)
- min_alpha = float - Learning rate will linearly drop to min_alpha as training progresses. To set it: alpha - (min_alpha * epochs) ~ 0.00
- negative = int - If > 0, negative sampling will be used, the int for negative specifies how many "noise words" should be drown. If set to 0, no negative sampling is used. - (5, 20)
- workers = int - Use these many worker threads to train the model (=faster training with multicore machines)

```
cores = multiprocessing.cpu_count() # Count the number of cores in a computer
w2v_model = Word2Vec(min_count=20,
                     window=2,
                     size=300,
                     sample=6e-5, 
                     alpha=0.03, 
                     min_alpha=0.0007, 
                     negative=20,
                     workers=cores-1)
```

### Building the Vocabulary Table:

Word2Vec requires us to build the vocabulary table (simply digesting all the words and filtering out the unique words, and doing some basic counts on them):
```
t = time()
w2v_model.build_vocab(sentences, progress_per=10000)
print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))
```

```
INFO - 01:16:19: collecting all words and their counts
(...)
Time to build vocab: 0.04 mins
```

### Training of the model:

**Parameters of the training:**

```
total_examples = int - Count of sentences;
epochs = int - Number of iterations (epochs) over the corpus - [10, 20, 30]
t = time()

w2v_model.train(sentences, total_examples=w2v_model.corpus_count, epochs=30, report_delay=1)

print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))
```

```
INFO - 01:16:22: training model with 3 workers on 3375 vocabulary and 300 features, using sg=0 hs=0 sample=6e-05 negative=20 window=2
(...)
Time to train the model: 1.26 mins
```

As we do not plan to train the model any further, we are calling init_sims(), which will make the model much more memory-efficient:

```
w2v_model.init_sims(replace=True)
INFO - 01:17:37: precomputing L2-norms of word weight vectors
```

## Exploring the model

```
w2v_model.wv.most_similar(positive=["homer_simpson"])
[('pleased', 0.6995885372161865),
 ('governor', 0.6765384078025818),
 ('congratulation', 0.673900842666626),
 ('select', 0.670538604259491),
 ('client', 0.6673898696899414),
 ('council', 0.6545040011405945),
 ('sir', 0.6534280776977539),
 ('easily', 0.652005672454834),
 ('montgomery_burn', 0.646706223487854),
 ('waylon', 0.6452118754386902)]
```

```
w2v_model.wv.most_similar(positive=["marge"])
[('homie', 0.691033124923706),
 ('grownup', 0.6905738115310669),
 ('anyhoo', 0.689124584197998),
 ('badly', 0.6851314306259155),
 ('homer', 0.6840591430664062),
 ('fault', 0.6757026314735413),
 ('rude', 0.6737008094787598),
 ('arrange', 0.6706920266151428),
 ('becky', 0.6677480936050415),
 ('depressed', 0.6599912643432617)]
```




## Références https://www.kaggle.com/pierremegret/gensim-word2vec-tutorial